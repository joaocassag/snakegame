
package snakegame;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.Random;
import javax.swing.JPanel;
import javax.swing.Timer;

/**
 *
 * @author joaocassamano
 */
public class GamePanel extends JPanel implements ActionListener {
    
    static final int WIDTH=600;
    static final int HEIGHT=600;
    static final int UNIT_SIZE=25;
    static final int GAME_UNITS = (WIDTH*HEIGHT)/UNIT_SIZE;
    static final int DELAY = 75;
    final int x[] = new int[GAME_UNITS];
    final int y[] = new int[GAME_UNITS];
    int bodyParts = 6;
    int applesEaten ;
    int appleX;
    int appleY;
    char direction='R';
    boolean running=false;
    Timer timer;
    Random random;
    
    GamePanel(){
        random = new Random();
        this.setPreferredSize(new Dimension(WIDTH,HEIGHT));
        this.setBackground(Color.black);
        this.setFocusable(true);
        this.addKeyListener(new SnakeGameKeyAdapter());
        startGame();
    }
    
    public void startGame(){
        newApple();
        running = true;
        timer = new Timer(DELAY,this);
        timer.start();
    }
    
    public void paintComponent(Graphics g){
        super.paintComponent(g);
        draw(g);
    }
    
    public void draw(Graphics g){
        //add Grid lines
        g.setColor(Color.blue);
        for(int i =0; i<HEIGHT/UNIT_SIZE;i++){
            g.drawLine(i*UNIT_SIZE, 0, i*UNIT_SIZE, HEIGHT);
            g.drawLine(0, i*UNIT_SIZE, WIDTH, i*UNIT_SIZE);
        }
        //g.setColor(Color.blue);
        
        if(running){
            //add apple
            g.setColor(Color.red);
            g.fillOval(appleX, appleY, UNIT_SIZE, UNIT_SIZE);

            //bodyParts
            for(int i =0; i<bodyParts;i++){
                if(i==0){
                    g.setColor(Color.green);
                    g.fillRect(x[i], y[i], UNIT_SIZE, UNIT_SIZE);
                } else{
                    g.setColor(new Color(45,180,0));
                    g.fillRect(x[i], y[i], UNIT_SIZE, UNIT_SIZE);
                }
            }
            
            g.setColor(Color.red);
            g.setFont(new Font("Times", Font.BOLD,40));
            FontMetrics metrics = getFontMetrics(g.getFont());
            g.drawString("Score: "+applesEaten, (WIDTH - metrics.stringWidth("Score: "+applesEaten))/2, g.getFont().getSize());
        } else{
            gameOver(g);
        }
    }
    
    public void newApple(){
        appleX = random.nextInt((int)WIDTH/UNIT_SIZE) * UNIT_SIZE;
        appleY = random.nextInt((int)WIDTH/UNIT_SIZE) * UNIT_SIZE;
    }
    
    public void move(){
        for(int i =bodyParts; i>0;i--){
            x[i] = x[i-1];
            y[i] = y[i-1];
        }
        
        switch(direction){
            case 'U':
                y[0] = y[0] - UNIT_SIZE;
                break;
            case 'D':
                y[0] = y[0] + UNIT_SIZE;
                break;
            case 'L':
                x[0] = x[0] - UNIT_SIZE;
                break;
            case 'R':
                x[0] = x[0] + UNIT_SIZE;
                break;
        }
    }
    
    public void checkApple(){
        if((x[0]==appleX) && (y[0]==appleY)){
            bodyParts++;
            applesEaten++;
            newApple();
        }
    }
    
    public void checkCollisions(){
        //collides with body of snake
        for(int i =bodyParts;i>0;i--){
            if((x[0]==x[i]) && (y[0]==y[i]) ){
                running = false;
            }
        }
        
        //head collides with left border
        if(x[0]<0){
            running = false;
        }
        //head collides with right border
        if(x[0]> WIDTH){
            running = false;
        }
        //head collides with top border
        if(y[0]<0){
            running = false;
        }
        //head collides with bottom border
        if(y[0]> HEIGHT){
            running = false;
        }
        
        if(!running){
            timer.stop();
        }
    
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if(running){
            move();
            checkApple();
            checkCollisions();
        }
        repaint();
    }

    private void gameOver(Graphics g) {
        
        g.setColor(Color.red);
        g.setFont(new Font("Times", Font.BOLD,40));
        FontMetrics metricsScore = getFontMetrics(g.getFont());
        g.drawString("Score: "+applesEaten, (WIDTH - metricsScore.stringWidth("Score: "+applesEaten))/2, g.getFont().getSize());
        
        g.setColor(Color.red);
        g.setFont(new Font("Times", Font.BOLD,75));
        FontMetrics metrics = getFontMetrics(g.getFont());
        g.drawString("Game Over", (WIDTH - metrics.stringWidth("Game Over"))/2, HEIGHT/2);
    }
    
    public class SnakeGameKeyAdapter extends KeyAdapter{
        
        @Override
        public void keyPressed(KeyEvent e){
            switch(e.getKeyCode()){
                case KeyEvent.VK_LEFT:
                    if(direction!='R'){
                        direction = 'L';
                    }
                    break;
                case KeyEvent.VK_RIGHT:
                    if(direction!='L'){
                        direction = 'R';
                    }
                    break;
                case KeyEvent.VK_UP:
                    if(direction!='D'){
                        direction = 'U';
                    }
                    break;
                case KeyEvent.VK_DOWN:
                    if(direction!='U'){
                        direction = 'D';
                    }
                    break;
            }
        }
    
    }
    
}
